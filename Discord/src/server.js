const discord = require('discord.js');
const client = new discord.Client();
const config = require('config');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs').promises;
const jsdom = require('jsdom');
const Ticket = require('./models/Ticket');
const { JSDOM } = jsdom;
const dom = new JSDOM();
const document = dom.window.document;
const db = require('./database/database');
const db_ = new sqlite3.Database('./db.sqlite');


client.on('ready', () => {
    console.log("Ready");
    client.user.setActivity('Checking tickets')
    db.authenticate().then(() => {
        console.log("Logged in to DB.");
        Ticket.init(db);
        Ticket.sync();
    }).catch(err => console.log(err));
});

client.on('raw', async payload => {
    let eventName = payload.t;
    if (eventName === 'MESSAGE_REACTION_ADD') {
        let msgId = payload.d.message_id;
        if (msgId === '665252129979760680') {
            let channelId = payload.d.channel_id;
            let channel = client.channels.cache.get(channelId);
            if (channel) {
                if (channel.messages.cache.has(msgId))
                    return;
                else {
                    try {
                        let msg = await channel.messages.fetch(msgId);
                        let reaction = msg.reactions.cache.get('🎫');
                        let user = client.users.get(payload.d.user_id);
                        client.emit('messageReactionAdd', reaction, user);
                    }
                    catch (error) {
                        console.log(error);
                        return;
                    }
                }
            }
        }
    }
});

client.on('message', async message => {

    let interval = null;
    interval = setInterval(channel_del, 10000);
    if (message.content == "!delete" && ((message.member.roles.cache.has('647143642955513890' || '704349666405384212') || message.member.roles.cache.has('655124739219652675' || '572900322985639937')))) {
        if (message.channel.name.startsWith('ticket')) {
            message.delete();
            let embed = new discord.MessageEmbed()
                .setDescription(`Deleting the channel in 10 seconds, Requested by ${message.author.username}`)
                .setColor("#CE2727")
            message.channel.send(embed)
        }
        else {
            message.channel.send('Did you really want to delete normal channel?')
        }
    }
    function channel_del() {
        let regex = new RegExp(/(ticket-[0-9]+)/);
        let channelName = message.channel.name;
        if (message.content.toLowerCase().startsWith("!delete") && regex.test(channelName) && ((message.member.roles.cache.has('647143642955513890' || '704349666405384212') || message.member.roles.cache.has('655124739219652675' || '572900322985639937')))) {
            message.channel.delete().then(c => console.log(c.name + " was deleted.")).catch(err => console.log(err));
            let channelId = message.channel.id;
            Ticket.update({ resolved: true }, { where: { channelId: channelId } })
            if (message.author.client) return;
            clearInterval(interval);
        }
    }
    if (message.content === "!close") {
        let filename = message.channel.name;
        await message.delete();
        let embed = new discord.MessageEmbed()
            .setDescription("Closing the ticket...saving transcript")
            .setColor("#CF3F3D")
        message.channel.send(embed)
        const test = message.channel.name;

        let messageCollection = new discord.Collection();
        let channelMessages = await message.channel.messages.fetch({
            limit: 100
        }).catch(err => console.log(err));

        messageCollection = messageCollection.concat(channelMessages);

        while (channelMessages.size === 100) {
            let lastMessageId = channelMessages.lastKey();
            channelMessages = await message.channel.messages.fetch({ limit: 100, before: lastMessageId }).catch(err => console.log(err));
            if (channelMessages)
                messageCollection = messageCollection.concat(channelMessages);
        }
        let msgs = messageCollection.array();
        msgs = msgs.sort((a, b) => a.createdTimestamp - b.createdTimestamp);
        let data = await fs.readFile(`./transcript.html`, 'utf8').catch(err => console.log(err));

        if (data) {
            await fs.writeFile(`transcript-${filename}.html`, data).catch(err => console.log(err));

            let guildElement = document.createElement('div');
            guildElement.className = "server-wrapper";
            let serverInfoWrapper = document.createElement('div');
            serverInfoWrapper.className = "server-info";

            let guildText = document.createTextNode(message.guild.name);

            let guildImg = document.createElement('img');
            guildImg.setAttribute('src', message.guild.iconURL() || "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png");
            guildImg.setAttribute('width', '150');
            guildElement.appendChild(guildImg);
            serverInfoWrapper.appendChild(guildText);
            guildElement.appendChild(serverInfoWrapper);

            await fs.appendFile(`transcript-${filename}.html`, guildElement.outerHTML).catch(err => console.log(err));

            let messages = "";
            msgs.forEach(async msg => {
                let parentContainer = document.createElement("div");
                parentContainer.className = "parent-container";

                let avatarDiv = document.createElement("div");
                avatarDiv.className = "avatar-container";
                let img = document.createElement('img');
                img.setAttribute('src', msg.author.avatarURL() || "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png");
                img.className = "avatar";
                avatarDiv.appendChild(img);

                parentContainer.appendChild(avatarDiv);

                let messageContainer = document.createElement('div');
                messageContainer.className = "message-container";

                let nameElement = document.createElement("span");
                let name = document.createTextNode(msg.author.tag + " " + msg.createdAt.toDateString() + " " + msg.createdAt.toLocaleTimeString());
                nameElement.appendChild(name);
                messageContainer.append(nameElement);

                if (msg.content.startsWith("```")) { // Append code block
                    let m = msg.content.replace(/```/g, "");
                    let codeNode = document.createElement("code");
                    let textNode = document.createTextNode(m);
                    codeNode.appendChild(textNode);
                    messageContainer.appendChild(codeNode);
                }
                else { // Append normal message
                    let msgNode = document.createElement('span');
                    let textNode = document.createTextNode(msg.content);
                    msgNode.append(textNode);
                    messageContainer.appendChild(msgNode);
                }

                // Append embed
                if (msg.embeds.length > 0) {
                    const embed = msg.embeds[0];

                    let container = document.createElement('div');
                    container.className = "embed-container";
                    let embedWrapper = document.createElement('div');
                    embedWrapper.className = "embed-wrapper";
                    let grid = document.createElement('div');
                    grid.className = "embed-grid";
                    let embedAuthor = document.createElement('div');
                    embedAuthor.className = "embed-author";
                    let embedTitle = document.createElement('div');
                    embedTitle.className = "embed-title";
                    let embedDescription = document.createElement('div');
                    embedDescription.className = "embed-description";


                    // Embed color
                    if (embed.color) {
                        embedWrapper.style.cssText = `border-color: #${embed.color.toString(16).padStart(6, '0')};`;
                    }

                    if (embed.avatar) {
                        // Add avatar
                        let img = document.createElement('img');
                        img.setAttribute('src', embed.author.avatarURL() || "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png");
                        img.className = "embed-author-icon";
                        embedAuthor.appendChild(img);

                        // Add author name
                        let authorNode = document.createElement('a');
                        let authorName = document.createTextNode(embed.avatar.username);
                        authorNode.append(authorName);
                        embedAuthor.appendChild(authorNode);
                    }

                    if (embed.title) {
                        let titleNode = document.createTextNode(embed.title);
                        embedTitle.append(titleNode);
                    }

                    if (embed.description) {
                        let descriptionNode = document.createTextNode(embed.description);
                        embedDescription.append(descriptionNode);
                    }

                    grid.appendChild(embedAuthor);
                    grid.appendChild(embedTitle);
                    grid.appendChild(embedDescription);
                    embedWrapper.appendChild(grid);
                    container.appendChild(embedWrapper);

                    messageContainer.appendChild(container);
                }

                parentContainer.appendChild(messageContainer);

                messages += parentContainer.outerHTML;
            })

            await fs.appendFile(`transcript-${filename}.html`, messages).catch(err => console.log(err));
        }

        client.channels.cache.get(config.channel).send({
            files: [`./transcript-${filename}.html`]
        });
        let sql = "SELECT authorId, today, tag, channel FROM Tickets WHERE channel = '" + test + "'";
        db_.all(sql, function (err, result) {
            if (err) throw err;
            for (let q = 0; q < result.length; q++) {
                let author = result[q].authorId;
                let today = result[q].today;
                let tag = result[q].tag;
                const info = new discord.MessageEmbed()
                    .setDescription('Ticket creator: ' + `<@${author}>` + '\nDate: ' + today + '\nFull nickname: ' + `**${tag}**`)
                    .setColor('#5EE781')
                    .setFooter('Ticket Bot 0.5')
                client.channels.cache.get(config.channel).send(info);
            }
        });
    }

    if (message.content.toLowerCase() === '!createticket' && message.member.hasPermission("ADMINISTRATOR")) {
        await message.delete();
        let embed = new discord.MessageEmbed()
            .setTitle('Support Ticket')
            .setDescription('React with the emoji to create the appeal')
            .setFooter("BArtis' Ticket Bot ver 0.5")
            .setTimestamp()
            .setColor("#1BD295");
        message.channel.send(embed)
            .then(msg => msg.react('🎫'))
    }

    if (message.content.startsWith.toLowerCase() === '!noroles' && !message.member.hasPermission("ADMINISTRATOR")) {
        message.reply("You don't have permission to use this command.")
        await message.delete({ timeout: "5000" })
    } else {
        for (let member of message.guild.members.cache.values()) {
            if (member.roles.cache.size <= 1) {
                let noRolesCount = 0;
                const counting = member.roles.cache.size
                if (counting <= 1) {
                    noRolesCount++
                }
                let embed = new discord.MessageEmbed()
                    .setColor(Math.random().toString(16).slice(2, 8).toUpperCase())
                    .setTitle('Kicking members without roles')
                    .setDescription(`Kicked ${noRolesCount} members`)
                message.channel.send(embed)
                await member.kick()
            }
        }
    }
});

client.on('messageReactionAdd', async (reaction, user) => {
    if (user.client) return;
    if (reaction.emoji.name === '🎫') {
        if (user.id == config.botID) {
            return;
        }
        else {
            reaction.users.remove(user.id)
        }

        let findTicket = await Ticket.findOne({ where: { authorId: user.id, resolved: false } }).catch(err => console.log(err));
        if (findTicket) {
            user.send("You already have a ticket opened!");
        }
        else {
            console.log("Create new ticket.")
            try {
                console.log("Creating ticket...");
                let channel = await reaction.message.guild.channels.create('ticket', {
                    type: 'text',
                    permissionOverwrites: [
                        {
                            allow: 'VIEW_CHANNEL',
                            id: user.id
                        },
                        {
                            allow: 'VIEW_CHANNEL',
                            id: config.id
                        },
                        {
                            deny: 'VIEW_CHANNEL',
                            id: reaction.message.guild.id
                        }
                    ]
                });
                // Create Embed Message and send it to channel.
                let embed = new discord.MessageEmbed()
                embed.setTitle(`Support Ticket`);
                embed.setDescription("type !close to close the ticket\nPlease provide your nickname to get the role");
                embed.setColor("#00FF4D");
                embed.setTimestamp();
                embed.setFooter("Ticket Bot");
                await channel.send(`Welcome <@${user.id}>`)
                let msg = await channel.send(embed);
                const now = new Date();
                const today = (`${now.toLocaleDateString('pl-PL')} ${now.toLocaleTimeString('pl-PL')}`);
                let newTicket = await Ticket.create({
                    authorId: user.id,
                    channelId: channel.id,
                    guildId: reaction.message.guild.id,
                    resolved: false,
                    closeMessageId: msg.id,
                    today: today,
                    tag: user.tag,
                    channel: channel.name
                });
                console.log("Ticket Saved...");
                let ticketId = String(newTicket.dataValues.ticketId).padStart(4, "0");
                await channel.edit({ name: `${channel.name}-${ticketId}` });
                let updated = channel.name
                Ticket.update({ channel: updated }, { where: { authorId: user.id } })
            }
            catch (ex) {
                console.log(ex);
            }
        }
    }
});

client.login(config.discordToken);